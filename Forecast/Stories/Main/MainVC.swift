//
//  MainVC.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/2/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

enum ForecastType: Int {
    case Day
    case FewDays
}

class MainVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet private weak var cloudyLabel: UILabel!
    @IBOutlet private weak var tempLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    
    // MARK: - Properties
    private var destinationForecas: ForecastVC! { didSet { configureModel() } }
    private var destinationChart: ChartVC!
    private var forecastDayModel = [ForecastList]()
    private var forecastFewDaysModel = [ForecastList]()
    var forecastModel: Forecast!
    var currentModel: CurrentWeather!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    // MARK: - Private functions
    private func configureUI() {
        view.frtGradient()
        cloudyLabel.text = currentModel.weather?.first?.main
        tempLabel.text = "\(Int(currentModel.main?.temp ?? 0))°"
        iconImageView.setFrom(name: currentModel.weather?.first?.icon ?? "")
    }
    
    private func configureModel() {
        let groups = Dictionary(grouping: forecastModel.list!) { (elem) -> Int in
            let date = Date(timeIntervalSince1970: elem.dt!)
            return Calendar.current.component(.day, from: date)
        }
        forecastFewDaysModel = groups.map {
            return $0.value.sorted { ($0.main?.temp ?? 0) > ($1.main?.temp ?? 0) }.first!
            }.sorted { $0.dt! < $1.dt! }
        forecastDayModel = Array(forecastModel.list?.prefix(8) ?? [])
    }
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination = segue.destination as? ForecastVC {
            destinationForecas = destination
            destination.forecastModel = forecastDayModel
        }
        if let destination = segue.destination as? ChartVC {
            destinationChart = destination
            destination.forecastModel = forecastDayModel
            destination.selectedChart = { [weak self] index in
                self?.destinationForecas.scrollToItemWith(index: index)
            }
        }
    }
    
    // MARK: - Actions
    @IBAction private func changeSegmentedControl(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            destinationForecas.forecastModel = forecastDayModel
            destinationForecas.type = .Day
            destinationChart.forecastModel = forecastDayModel
            destinationChart.type = .Day
        default:
            destinationForecas.forecastModel = forecastFewDaysModel
            destinationForecas.type = .FewDays
            destinationChart.forecastModel = forecastFewDaysModel
            destinationChart.type = .FewDays
        }
    }
}

