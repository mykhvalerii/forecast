//
//  ForecastCVC.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

class ForecastVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var forecastModel = [ForecastList]()
    var type: ForecastType = .Day { didSet { changeState() } }
    
    // MARK: - Functions
    private func changeState() {
        forecastModel.count > 0 ? collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true) : nil
        collectionView.reloadData()
    }
    
    func scrollToItemWith(index: Int) {
        if collectionView.numberOfItems(inSection: 0) >= index {
            collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
        }
    }
}

// MARK: - UICollectionViewDataSource
extension ForecastVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecastModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kForecastCell", for: indexPath) as! ForecastCell
        if forecastModel.count >= indexPath.row {
            let model = forecastModel[indexPath.row]
            type == .Day ? cell.configureDayWith(model: model) : cell.configureFewDaysWith(model: model)
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ForecastVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        view.layoutIfNeeded()
        let widht = self.view.frame.width / 4.5 - 10
        let height = self.view.frame.height
        return CGSize(width: widht, height: height)
    }
}


