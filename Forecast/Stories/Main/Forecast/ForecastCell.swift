//
//  ForecastCell.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit
import AlamofireImage

class ForecastCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var bottomLabel: UILabel!
    
    // MARK: - Life cycle
    override func prepareForReuse() {
        super.prepareForReuse()
        topLabel.text = nil
        iconImageView.image = UIImage()
        bottomLabel.text = nil
    }
    
    // MARK: - Functions
    func configureDayWith(model: ForecastList) {
        if let timestamp = model.dt {
            topLabel.text = String(timestamp).hour
        }
        iconImageView.setFrom(name: model.weather?.first?.icon ?? "")
        bottomLabel.text = "\(Int(model.main?.temp ?? 0))°"
    }
    
    func configureFewDaysWith(model: ForecastList) {
        if let timestamp = model.dt {
            topLabel.text = String(timestamp).dayOfWeek
        }
        iconImageView.setFrom(name: model.weather?.first?.icon ?? "")
        bottomLabel.text = "\(Int(model.main?.temp ?? 0))°"
    }
}
