//
//  ChartCell.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/4/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

class ChartCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var chartView: UIView!
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet private weak var topConstraint: NSLayoutConstraint!
    
    // MARK: - Life cycle
    override func prepareForReuse() {
        super.prepareForReuse()
        dayLabel.text = nil
        topConstraint.constant = 0
    }
    
    // MARK: - Functions
    func configureChart(model: ForecastList, minTemp: Double, maxTemp: Double, type: ForecastType) {
        layoutIfNeeded()
        if let timestamp = model.dt {
            type == .Day ? (dayLabel.text = String(timestamp).hour) : (dayLabel.text = String(timestamp).dayOfWeek)
        }
        guard let temp = model.main?.temp else { return }
        let height = contentView.frame.height - 30
        let tempS = CGFloat(maxTemp - minTemp)
        topConstraint.constant = height - (height / tempS * CGFloat(temp - minTemp))
        layoutIfNeeded()
    }
}
