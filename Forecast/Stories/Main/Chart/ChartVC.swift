//
//  ChartVC.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/4/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

// ТУТ Я УЖЕ СОВСЕМ ЗАКОНЧИЛСЯ 
class ChartVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private var tempLabels: [UILabel]!
    
    // MARK: - Properties
    private var minTemp: Double = 0
    private var maxTemp: Double = 0
    var selectedChart: ((Int) -> ())?
    var forecastModel = [ForecastList]()
    var type: ForecastType = .Day { didSet { changeState() } }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        changeState()
    }
    
    // MARK: - Functions
    private func changeState() {
        let temp = forecastModel.map { $0.main?.temp }
        let sortedTemp = temp.sorted { ($0 ?? 0) > ($1 ?? 0) }
        
        minTemp = sortedTemp.last! ?? 0
        maxTemp = sortedTemp.first! ?? 0
        
        let averageTemp = (maxTemp - minTemp) / Double(tempLabels.count - 1)
        var tmp = minTemp
        
        tempLabels.forEach {
            $0.text = String(format: "%.2f", tmp)
            tmp += averageTemp
        }
        forecastModel.count > 0 ? collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true) : nil
        collectionView.reloadData()
    }
}

// MARK: - UICollectionViewDataSource
extension ChartVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return forecastModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kChartCell", for: indexPath) as! ChartCell
        if forecastModel.count >= indexPath.row {
            cell.configureChart(model: forecastModel[indexPath.row], minTemp: minTemp, maxTemp: maxTemp, type: type)
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ChartVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        view.layoutSubviews()
        let widht: CGFloat = type == .Day ? 40 : 80
        let height = view.frame.height
        return CGSize(width: widht, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedChart?(indexPath.row)
    }
}


