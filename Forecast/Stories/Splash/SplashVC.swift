//
//  SplashVC.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    // MARK: - Properties
    private var forecastModel: Forecast?
    private var currentModel: CurrentWeather?
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
    }
    
    // MARK: - Functions
    private func configureUI() {
        getForecast(completed: { [weak self] in
            self?.showMainScreen()
        })
        getWeather { [weak self] in
            self?.showMainScreen()
        }
    }
    
    private func showMainScreen() {
        guard forecastModel != nil else { return }
        guard currentModel != nil else { return }
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! MainVC
        destination.forecastModel = forecastModel
        destination.currentModel = currentModel
        show(destination, sender: self)
    }
}

// MARK: - API
extension SplashVC {
    
    private func getForecast(completed: @escaping () -> ()) {
        ApiManager.Router.GetForecast(city: "kyiv", units: "metric").request { [weak self] (response) in
            
            let cache = ApiManager.Router.GetForecast(city: "kyiv", units: "metric").getCache()
            self?.forecastModel = cache.value
            
            guard response.error == nil else {
                self?.setError(text: response.error?.localizedDescription ?? "error")
                completed()
                return
            }
            
            guard String(response.result.value?.cod ?? 0) == "200" else {
                self?.setError(text: String(response.result.value?.message ?? 0))
                completed()
                return
            }
            
            guard let model = response.result.value else { return }
            self?.forecastModel = model
            completed()
        }
    }
    
    private func getForecastFromCashe(completed: @escaping () -> ()) {
        
    }
    
    private func getWeather(completed: @escaping () -> ()) {
        ApiManager.Router.GetWeather(city: "kyiv", units: "metric").request { [weak self] (response) in
            
            let cache = ApiManager.Router.GetWeather(city: "kyiv", units: "metric").getCache()
            self?.currentModel = cache.value
            
            guard response.error == nil else {
                self?.setError(text: response.error?.localizedDescription ?? "error")
                completed()
                return
            }
            
            guard let model = response.result.value else { return }
            self?.currentModel = model
            completed()
        }
    }
}
