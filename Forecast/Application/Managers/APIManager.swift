//
//  APIManager.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestObject: URLRequestConvertible, Encodable {
    
    associatedtype ResponseObject: Decodable
    
    var pathGroups: [String] { get }
    var queryItems: [URLQueryItem] { get }
    var additionalHeaders: HTTPHeaders { get }
    var method: HTTPMethod { get }
    var endpoint: String { get }
}

extension RequestObject {
    
    var additionalHeaders: HTTPHeaders { return ApiManager.State.header }
    var method: HTTPMethod { return .get }
    
    
    func request(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseObject>) -> Void) {
        
        let coder = JSONDecoder()
        coder.dateDecodingStrategy = .secondsSince1970
        Alamofire.request(self).responseInternal(type: ResponseObject.self, queue: queue, coder: coder) { (callback) in
            completionHandler(callback)
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var contextURL = ApiManager.State.domain.appendingPathComponent(endpoint, isDirectory: false)
        for group in pathGroups {
            contextURL = contextURL.appendingPathComponent(group, isDirectory: true)
        }
        
        var components = URLComponents(url: contextURL, resolvingAgainstBaseURL: false)!
        components.queryItems = queryItems
        
        var request = try URLRequest(url: components.url!, method: method, headers: additionalHeaders)
        request.cachePolicy = NSURLRequest.CachePolicy.returnCacheDataElseLoad
        request = try URLEncoding.default.encode(request, with: nil)
        return request
    }
    
    func getCache() -> Result<ResponseObject> {
        return Result<ResponseObject>.init(value: { () -> ResponseObject in
            let coder = JSONDecoder()
            coder.dateDecodingStrategy = .secondsSince1970
            let cache = URLCache.shared.cachedResponse(for: try self.asURLRequest())
            if let data = cache?.data {
                return try coder.decode(ResponseObject.self, from: data)
            }
            throw AFError
                .responseSerializationFailed(reason: .inputDataNilOrZeroLength)
        })
    }
}

struct ApiManager {
    
    struct Router { }
    
    struct State {
        static let domain = URL(string: "https://community-open-weather-map.p.rapidapi.com")!
        static let imageDomain = URL(string: "https://openweathermap.org/img/w")!
        static let header: HTTPHeaders = ["X-RapidAPI-Key": "8160a8e729msh8ac9d4397dfe0f6p1f6dc9jsnb92cf952eec0"]
    }
}

