//
//  APIForecastModel.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import Foundation

struct Forecast: Codable {
    
    let city: City?
    let cnt: Int?
    let cod: Int?
    let list: [ForecastList]?
    let message: Float?
    
    enum CodingKeys: String, CodingKey {
        case city = "city"
        case cnt = "cnt"
        case cod = "cod"
        case list = "list"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        city = try values.decodeIfPresent(City.self, forKey: .city)
        cnt = try values.decodeIfPresent(Int.self, forKey: .cnt)
        cod = (try? values.decode(Int.self, forKey: .cod)) ?? Int((try? values.decode(String.self, forKey: .cod)) ?? "")
        list = try values.decodeIfPresent([ForecastList].self, forKey: .list)
        message = (try? values.decode(Float.self, forKey: .cod)) ?? Float((try? values.decode(String.self, forKey: .cod)) ?? "")
    }
}

struct ForecastList: Codable {
    
    let clouds: Cloud?
    let dt: Double?
    let dtTxt: String?
    let main: Main?
    let weather: [Weather]?
    let wind: Wind?
    var averageTemp: Double?
    
    enum CodingKeys: String, CodingKey {
        case clouds = "clouds"
        case dt = "dt"
        case dtTxt = "dt_txt"
        case main = "main"
        case weather = "weather"
        case wind = "wind"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        clouds = try values.decodeIfPresent(Cloud.self, forKey: .clouds)
        dt = try values.decodeIfPresent(Double.self, forKey: .dt)
        dtTxt = try values.decodeIfPresent(String.self, forKey: .dtTxt)
        main = try values.decodeIfPresent(Main.self, forKey: .main)
        weather = try values.decodeIfPresent([Weather].self, forKey: .weather)
        wind = try values.decodeIfPresent(Wind.self, forKey: .wind)
    }
    
}

struct Wind: Codable {
    
    let deg: Float?
    let speed: Float?
    
    enum CodingKeys: String, CodingKey {
        case deg = "deg"
        case speed = "speed"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        deg = try values.decodeIfPresent(Float.self, forKey: .deg)
        speed = try values.decodeIfPresent(Float.self, forKey: .speed)
    }
    
}

struct Weather: Codable {
    
    let descriptionField: String?
    let icon: String?
    let id: Int?
    let main: String?
    
    enum CodingKeys: String, CodingKey {
        case descriptionField = "description"
        case icon = "icon"
        case id = "id"
        case main = "main"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        main = try values.decodeIfPresent(String.self, forKey: .main)
    }
    
}

struct Main: Codable {
    
    let grndLevel: Float?
    let humidity: Int?
    let pressure: Float?
    let seaLevel: Float?
    let temp: Double?
    let tempKf: Double?
    let tempMax: Float?
    let tempMin: Float?
    
    enum CodingKeys: String, CodingKey {
        case grndLevel = "grnd_level"
        case humidity = "humidity"
        case pressure = "pressure"
        case seaLevel = "sea_level"
        case temp = "temp"
        case tempKf = "temp_kf"
        case tempMax = "temp_max"
        case tempMin = "temp_min"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        grndLevel = try values.decodeIfPresent(Float.self, forKey: .grndLevel)
        humidity = try values.decodeIfPresent(Int.self, forKey: .humidity)
        pressure = try values.decodeIfPresent(Float.self, forKey: .pressure)
        seaLevel = try values.decodeIfPresent(Float.self, forKey: .seaLevel)
        temp = try values.decodeIfPresent(Double.self, forKey: .temp)
        tempKf = try values.decodeIfPresent(Double.self, forKey: .tempKf)
        tempMax = try values.decodeIfPresent(Float.self, forKey: .tempMax)
        tempMin = try values.decodeIfPresent(Float.self, forKey: .tempMin)
    }
    
}

struct Cloud: Codable {
    
    let all: Int?
    
    enum CodingKeys: String, CodingKey {
        case all = "all"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        all = try values.decodeIfPresent(Int.self, forKey: .all)
    }
    
}

struct City: Codable {
    
    let coord: Coord?
    let country: String?
    let id: Int?
    let name: String?
    let population: Int?
    
    enum CodingKeys: String, CodingKey {
        case coord = "coord"
        case country = "country"
        case id = "id"
        case name = "name"
        case population = "population"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        coord = try values.decodeIfPresent(Coord.self, forKey: .coord)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        population = try values.decodeIfPresent(Int.self, forKey: .population)
    }
    
}

struct Coord: Codable {
    
    let lat: Float?
    let lon: Float?
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lon = "lon"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Float.self, forKey: .lat)
        lon = try values.decodeIfPresent(Float.self, forKey: .lon)
    }
    
}
