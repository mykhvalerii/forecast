//
//  APIWeatherModel.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/4/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import Foundation

struct CurrentWeather: Codable {
    
    let cod: Int?
    let main: Main?
    let weather: [WeatherModel]?
    let wind: Wind?
    
    enum CodingKeys: String, CodingKey {
        case cod = "cod"
        case main = "main"
        case weather = "weather"
        case wind = "wind"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cod = try values.decodeIfPresent(Int.self, forKey: .cod)
        main = try values.decodeIfPresent(Main.self, forKey: .main)
        weather = try values.decodeIfPresent([WeatherModel].self, forKey: .weather)
        wind = try values.decodeIfPresent(Wind.self, forKey: .wind)
    }
}

struct WindModel: Codable {
    
    let deg: Int?
    let speed: Int?
    
    enum CodingKeys: String, CodingKey {
        case deg = "deg"
        case speed = "speed"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        deg = try values.decodeIfPresent(Int.self, forKey: .deg)
        speed = try values.decodeIfPresent(Int.self, forKey: .speed)
    }
    
}

struct WeatherModel: Codable {
    
    let descriptionField: String?
    let icon: String?
    let id: Int?
    let main: String?
    
    enum CodingKeys: String, CodingKey {
        case descriptionField = "description"
        case icon = "icon"
        case id = "id"
        case main = "main"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        main = try values.decodeIfPresent(String.self, forKey: .main)
    }
    
}

struct CurrentMain: Codable {
    
    let humidity: Int?
    let pressure: Int?
    let temp: Float?
    let tempMax: Float?
    let tempMin: Int?
    
    enum CodingKeys: String, CodingKey {
        case humidity = "humidity"
        case pressure = "pressure"
        case temp = "temp"
        case tempMax = "temp_max"
        case tempMin = "temp_min"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        humidity = try values.decodeIfPresent(Int.self, forKey: .humidity)
        pressure = try values.decodeIfPresent(Int.self, forKey: .pressure)
        temp = try values.decodeIfPresent(Float.self, forKey: .temp)
        tempMax = try values.decodeIfPresent(Float.self, forKey: .tempMax)
        tempMin = try values.decodeIfPresent(Int.self, forKey: .tempMin)
    }
    
}
