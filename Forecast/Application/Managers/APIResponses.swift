//
//  APIResponses.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import Foundation

extension ApiManager.Router {
    
    struct GetForecast: RequestObject {
        
        typealias ResponseObject = Forecast
        
        var city: String
        var units: String
        var queryItems: [URLQueryItem] {
            return [URLQueryItem(name: "q", value: city),
                    URLQueryItem(name: "units", value: units)]
        }
        var pathGroups: [String] { return [] }
        var endpoint: String { return "forecast" }
    }
    
    struct GetWeather: RequestObject {
        
        typealias ResponseObject = CurrentWeather
        
        var city: String
        var units: String
        var queryItems: [URLQueryItem] {
            return [URLQueryItem(name: "q", value: city),
                    URLQueryItem(name: "units", value: units)]
        }
        var pathGroups: [String] { return [] }
        var endpoint: String { return "weather" }
    }
    
}
