//
//  AppDelegate.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/2/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        return true
    }
}

