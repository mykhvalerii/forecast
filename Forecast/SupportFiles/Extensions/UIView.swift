//
//  UIView.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/2/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

extension UIView {
    
    func frtGradient() {
        
        layoutIfNeeded()
        
        let topColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1).cgColor
        let bottomColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor, bottomColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
