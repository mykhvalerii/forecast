//
//  UIImageView.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView {
    
    func setFrom(name: String) {
        let url = ApiManager.State.imageDomain.appendingPathComponent("\(name).png")
        self.af_setImage(withURL: url)
    }
}
