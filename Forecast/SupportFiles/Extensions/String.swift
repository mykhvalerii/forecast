//
//  String.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import Foundation

extension String {
    
    var dayOfWeek: String {
        guard let timestamp = Double(self) else { return "" }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        
        let date = Date(timeIntervalSince1970: timestamp)
        
        return dateFormatter.string(from: date)
    }
    
    var hour: String {
        guard let timestamp = Double(self) else { return "" }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "h a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        let date = Date(timeIntervalSince1970: timestamp)
        
        return dateFormatter.string(from: date)
    }
}
