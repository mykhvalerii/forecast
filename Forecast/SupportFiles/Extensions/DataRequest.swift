//
//  DataRequest.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import Alamofire

extension DataRequest {
    
    fileprivate func APICodableResponseSerializer<T: Decodable>(_ type: T.Type, coder: JSONDecoder = JSONDecoder()) -> DataResponseSerializer<T> {
        return DataResponseSerializer.init(serializeResponse: {
            (urlRequest, httpUrlResponse, data, error) -> Result<T> in
            
            if let request = urlRequest {
                print("response: \(request)\n\nerror: \(error?.localizedDescription ?? "nil")\n")
            }
            
            guard error == nil else {
                return .failure(error!)
            }
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result {
                return try coder.decode(T.self, from: data)
            }
        })
    }
    
    @discardableResult
    open func responseInternal<T: Decodable>(type: T.Type,
                                             queue: DispatchQueue? = nil,
                                             coder: JSONDecoder = JSONDecoder(),
                                             completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: APICodableResponseSerializer(T.self), completionHandler: { (response) in
            print(response.result)
            completionHandler(response)
        })
    }
}

