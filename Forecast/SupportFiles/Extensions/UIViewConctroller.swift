//
//  UIViewConctroller.swift
//  Forecast
//
//  Created by Valerii Mykhailenko on 3/3/19.
//  Copyright © 2019 Valerii Mykhailenko. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setError(text: String) {
        
        let errorContainer = UIView()
        errorContainer.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        errorContainer.alpha = 0
        errorContainer.layer.cornerRadius = 10
        errorContainer.clipsToBounds = true
        errorContainer.isUserInteractionEnabled = false
        
        let errorLabel = UILabel()
        errorLabel.textColor = .white
        errorLabel.textAlignment = .center
        errorLabel.font.withSize(12)
        errorLabel.text = text
        errorLabel.clipsToBounds = true
        errorLabel.numberOfLines = 0
        errorLabel.isUserInteractionEnabled = false
        
        errorContainer.addSubview(errorLabel)
        self.view.addSubview(errorContainer)
        
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorContainer.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: errorLabel, attribute: .leading, relatedBy: .equal, toItem: errorContainer, attribute: .leading, multiplier: 1, constant: 15).isActive = true
        NSLayoutConstraint(item: errorLabel, attribute: .trailing, relatedBy: .equal, toItem: errorContainer, attribute: .trailing, multiplier: 1, constant: -15).isActive = true
        NSLayoutConstraint(item: errorLabel, attribute: .bottom, relatedBy: .equal, toItem: errorContainer, attribute: .bottom, multiplier: 1, constant: -15).isActive = true
        NSLayoutConstraint(item: errorLabel, attribute: .top, relatedBy: .equal, toItem: errorContainer, attribute: .top, multiplier: 1, constant: 15).isActive = true
        
        NSLayoutConstraint(item: errorContainer, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 65).isActive = true
        NSLayoutConstraint(item: errorContainer, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -65).isActive = true
        NSLayoutConstraint(item: errorContainer, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 75).isActive = true
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            errorContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                errorContainer.alpha = 0.0
            }, completion: { _ in
                errorContainer.removeFromSuperview()
            })
        })
    }
}
